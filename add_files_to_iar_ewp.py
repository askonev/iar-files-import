import os
import argparse
from lxml import etree
from fnmatch import fnmatch
from typing import Iterable
from typing import Dict

NAME_TAG = "name"
GROUP_TAG = "group"
FILE_TAG = "file"
PROJECT_TAG = "project"
IAR_PROJECT_DIR = "$PROJ_DIR$"


def get_next_file_path(project_path: str,
                       files_directory_path: str,
                       pattern: str = "*.c") -> Iterable[str]:
    """
    Generates file relative path
    :param project_path: absolute path to project
    :param files_directory_path: absolute path to directory with files
    :param pattern: Unix filename pattern matching
    :return: file generator
    """
    for path, dir_list, file_list in os.walk(files_directory_path):
        for f in file_list:
            if fnmatch(f, pattern):
                yield os.path.join(os.path.relpath(path, project_path), f)


def get_iar_file_text(file_relative_path: str) -> str:
    """
    Get the file description text for name element
    :param file_relative_path: relative to project directory path to file
    :return: text for name element
    """
    return os.path.join(IAR_PROJECT_DIR, file_relative_path)


def append_child(element: object, tag: object, name: str) -> object:
    """
    Append a new element to a tree
    :param element: element to append as child to
    :param tag: tag name
    :param name: object name
    :return: newly appended element
    """
    new_element = etree.SubElement(element, tag)
    etree.SubElement(new_element, NAME_TAG).text = name

    return new_element


def write_tree(tree: object, path: str, encoding="utf-8", pretty=True) -> None:
    """
    Write the tree to a file
    :param tree: lxml ElementTree or element
    :param path: path to the file
    :param encoding: file encoding
    :return: None
    """
    etree.indent(tree)
    with open(path, 'wb') as proj:
        proj.write(etree.tostring(tree,
                                  encoding=encoding,
                                  pretty_print=pretty,
                                  xml_declaration=True))


def is_file_node_exists(file_relative_path: str, element: object) -> bool:
    """
    Check if file is already in the project
    :param file_relative_path: file path relative to project path
    :param element: lxml tree element
    :return: True - file is imported, False - otherwise
    """
    file = os.path.basename(file_relative_path)
    xpath = f"./file/name[contains(text(), '{file}')]"
    return len(element.xpath(xpath)) != 0


def add_file(tree: object, file_relative_path: str) -> None:
    """
    Adds a file to a tree
    :param tree: ElementTree or element
    :param file_relative_path: file relative to project directory path
    :return: None
    """
    dir_path, file = os.path.split(file_relative_path)
    context = tree.xpath(f"/{PROJECT_TAG}")[0]
    element = None
    for name in dir_path.split('\\'):
        try:
            xpath = f"./{GROUP_TAG}/{NAME_TAG}[text() = '{name}']/parent::{GROUP_TAG}"
            element = context.xpath(xpath)[0]
        except IndexError:
            element = append_child(context, GROUP_TAG, name)
            print(f"Added the group \"{name}\" from \"{dir_path}\"")
        except Exception as e:
            print(f"Unable to continue: " + str(e))
            exit(-1)
        context = element

    if not is_file_node_exists(file_relative_path, context):
        append_child(context, FILE_TAG, get_iar_file_text(file_relative_path))
        print(f"Added the file \"{file_relative_path}\"")


def add_files_to_tree(tree: object, files: Iterable[str]) -> None:
    """
    Adds files to a tree
    :param tree: lxlm ElementTree or Element
    :param files: iterable with files relative to project directory path
    :return:
    """
    for file_path in files:
        add_file(tree, file_path)


def main(args: Dict[str, str]) -> None:
    project_path = args[PROJECT_PATH_ID]
    files_dir_path = args[PATH_ID]
    pattern = args[PATTERN_ID]

    project_dir, project_name = os.path.split(project_path)

    tree = etree.parse(project_path)
    add_files_to_tree(tree.getroot(),
                      get_next_file_path(project_dir, files_dir_path, pattern))
    write_tree(tree, project_path, encoding=tree.docinfo.encoding)


if __name__ == '__main__':
    PATH_ID = "Path"
    PATTERN_ID = "Pattern"
    PROJECT_PATH_ID = "Project_dir"
    FIND_ALL_FILES = "*.*"

    parser = argparse.ArgumentParser(description="Import the files into the "
                                                 "IAR project file following "
                                                 "the directory tree.")

    parser.add_argument(PATH_ID,
                        metavar='<path>',
                        type=str,
                        help='A path to the source directory to be added')

    parser.add_argument(PROJECT_PATH_ID,
                        metavar='<project_path>',
                        type=str,
                        help='A path to the project.')

    parser.add_argument("-p",
                        "--pattern",
                        type=str,
                        help='Unix filename pattern matching',
                        default=FIND_ALL_FILES,
                        dest=PATTERN_ID)

    main(vars(parser.parse_args()))
